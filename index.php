<?php
include 'db.php';

$dbhost = 'localhost';
$dbuser = 'root';
$dbpass = 'root';
$dbname = 'library';

$db = new db($dbhost, $dbuser, $dbpass, $dbname);

$accounts = $db->query('SELECT * FROM mytable')->fetchAll();

foreach ($accounts as $account) {
    echo $account['name'] . '|' . $account['text'] . '<br>';
}

?>